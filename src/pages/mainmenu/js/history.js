const columns = [
  {
    name: 'no',
    required: true,
    label: 'No',
    align: 'center',
    field: row => row.name,
    format: val => `${val}`,
    sortable: true
  },
  { name: 'employeeid', align: 'center', label: 'Employee ID', field: 'employeeid', sortable: true, align: 'center' },
  { name: 'callsign', label: 'Callsign', field: 'callsign', sortable: true, align: 'center' },
  { name: 'nikktp', label: 'NIK KTP', field: 'nikktp', align: 'center' },
  { name: 'nik', label: 'NIK', field: 'nik', align: 'center' },
  { name: 'company', label: 'Company Name', field: 'company', align: 'center' },
  { name: 'regional', label: 'Regional', field: 'regional', sortable: true, align: 'center' },
  { name: 'division', label: 'Division', field: 'division', sortable: true, align: 'center' },
  { name: 'mainjob', label: 'Main Job', field: 'mainjob', sortable: true, align: 'center' },
  { name: 'position', label: 'Position', field: 'position', sortable: true, align: 'center' },
  { name: 'level', label: 'Level', field: 'level', sortable: true, align: 'center' },
  { name: 'email', label: 'Email', field: 'email', sortable: true, align: 'center' },
  { name: 'address', label: 'Address', field: 'address', sortable: true, align: 'center' },
  { name: 'education', label: 'Education', field: 'education', sortable: true, align: 'center' },
  { name: 'phonenum', label: 'Phone Number', field: 'phonenum', sortable: true, align: 'center' },
  { name: 'resigndate', label: 'Resign Date', field: 'resigndate', sortable: true, align: 'center' },
  { name: 'remark_resign', label: 'Resign Remark', field: 'remark_resign', sortable: true, align: 'center' },
  { name: 'replacementemployeeid', label: 'Action', field: 'replacementemployeeid', sortable: true, align: 'center' }
]

export default {
  data () {
    return {
      role: localStorage.getItem('user-role'),
      iconreplacement: false,
      iconview: false,
      filter: '',
      startdate: '',
      enddate: '',
      company: null,
      oldid: '',
      oldname: '',
      oldcallsign: '',
      oldnik: '',
      selectCompany: null,

      employeeid: '',
      callsign: '',
      nik: '',
      nik_ktp: '',
      fullname: '',
      company: '',
      regional: '',
      division: '',
      mainjob: '',
      position: '',
      joindate: '',
      joincallsign: '',
      releasecallsign: '',
      address: '',
      education: '',
      level: '',
      phonenum: '',
      email: '',
      remark: null,

      detail: {
        employeeid: '',
        callsign: '',
        nik: '',
        nik_ktp: '',
        fullname: '',
        company: '',
        regional: '',
        division: '',
        mainjob: '',
        position: '',
        joindate: '',
        joincallsign: '',
        releasecallsign: '',
        address: '',
        education: '',
        level: '',
        phonenum: '',
        email: '',
        remark: null,
      },

      columns,
      rows: [],
      optionsCompany: [],
      optionsCompanyFilter: [],
      optionsRegional: [],
      optionsDivision: [],
      optionsPosition: [],
      optionsMainjob: [],
      optionsEducation: [],
      optionsLevel: []
    }
  },
  methods: {
    getEmployees(){
      var start = ''
      var end = ''
      if (this.selectCompany != null) {
        var selected = this.selectCompany.companyname
      } else {
        var selected = ''
      }
      if (this.startdate != ''){
        start = this.formatDate(this.startdate)
      }
      if (this.enddate != ''){
        end = this.formatDate(this.enddate)
      }
      this.$axios.get('/employee/getallresignemployeewithfilter', {
        headers: {
          Authorization : localStorage.getItem('user-token')
        },
        params:{
          'startdate' : start,
          'enddate' : end,
          'company': selected
        }
      })
      .then(response => {
        var datas = response.data
        var i = 0;
        for (var key in datas) {
          datas[i]['no'] = i + 1
          i++
        }
        this.rows = datas
      })
      .catch(error => {
        console.log('message : ' + error)
      })
    },
    getDivisionList(){
      this.$axios.get('/master/getalldivision',{
        headers: {
          Authorization : localStorage.getItem("user-token")
        }
      })
      .then(response => {
        this.optionsDivision = response.data
      })
      .catch(error => {
        console.log('message : ' + error)
      })
    },
    getRegionalList(){
      this.$axios.get('/master/getallregional',{
        headers: {
          Authorization : localStorage.getItem("user-token")
        }
      })
      .then(response => {
        this.optionsRegional = response.data
      })
      .catch(error => {
        console.log('message : ' + error)
      })
    },
    getPositionList(){
      this.$axios.get('/master/getallposition',{
        headers: {
          Authorization : localStorage.getItem("user-token")
        }
      })
      .then(response => {
        this.optionsPosition = response.data
      })
      .catch(error => {
        console.log('message : ' + error)
      })
    },
    getMainjobList(){
      this.$axios.get('/master/getallmainjob',{
        headers: {
          Authorization : localStorage.getItem("user-token")
        }
      })
      .then(response => {
        this.optionsMainjob = response.data
      })
      .catch(error => {
        console.log('message : ' + error)
      })
    },
    getLevelList(){
      this.$axios.get('/master/getalllevel',{
        headers: {
          Authorization : localStorage.getItem("user-token")
        }
      })
      .then(response => {
        this.optionsLevel = response.data
      })
      .catch(error => {
        console.log('message : ' + error)
      })
    },
    getCompanyList(){
      this.$axios.get('/master/getcompany',{
        headers: {
          Authorization : localStorage.getItem("user-token")
        }
      })
      .then(response => {
        this.optionsCompany = response.data
        this.optionsCompanyFilter = response.data
      })
      .catch(error => {
        console.log('message : ' + error)
      })
    },
    getEducationList(){
      this.$axios.get('/master/getalledu',{
        headers: {
          Authorization : localStorage.getItem("user-token")
        }
      })
      .then(response => {
        this.optionsEducation = response.data
      })
      .catch(error => {
        console.log('message : ' + error)
      })
    },
    filterSearch(){
      this.$q.loading.show()
      if (this.startdate != null && this.enddate != null) {
        this.$q.loading.hide()
        this.getEmployees()
      } else {
        this.$q.loading.hide()
        this.$q.notify({
          message: 'Please Complete the Date Periode',
          color: 'red',
          icon: 'warning'
        })
      }
    },
    resetFilter(){
      this.selectCompany = null
      this.startdate = ''
      this.enddate = ''
      this.getEmployees()
    },
    viewReplace(id){
      this.$axios.get('/employee/getemployeebyid', {
        headers: {
            Authorization : localStorage.getItem('user-token'),
            "Access-Control-Allow-Origin" : "http://localhost:8080"
        },
        params : {
            'id' : id
        }
      })
      .then(response => {
          if (response.data != '') {
              this.detailsData(response.data)
              this.iconview = true
          } else {
              this.$q.notify({
                  message: 'Employee Data Not Found',
                  color: 'red',
                  icon: 'warning'
              })
              this.getEmployees()
          }
      })
      .catch(error => {
          console.log('message : ' + error)
          this.$q.notify({
              message: error,
              color: 'red',
              icon: 'warning'
          })
          this.getEmployees()
      })
    },
    addReplace(id, name, callsign, nik){
      this.iconreplacement = true
      this.replacementemployeeid = id
      this.oldid = id
      this.oldname = name
      this.oldcallsign = callsign
      this.oldnik = nik
    },
    onReset(){
      this.callsign = null
      this.nik_ktp = null
      this.nik = null
      this.fullname = null
      this.education = null
      this.address = null
      this.joindate = null
      this.phonenum = null
      this.regional = null
      this.division = null
      this.mainjob = null
      this.position = null
      this.level = null
      this.joindate = null
      this.company = null
      this.email = null
      this.remark = null
      
    },
    onSubmit() {        
      this.$q.loading.show()
      if (this.callsign != '' && this.nik_ktp != '' && this.nik != '' && this.fullname != '' && this.address != '' && this.education != '' && this.phonenum != '' && this.regional != '' && this.division != '' && this.mainjob != '' && this.position != '' && this.level != '' && this.joindate != '' && this.company != '' && this.email != ''){
        var body = {
          'employeeid' : null,
          'callsign' : this.callsign,
          'nikktp' : this.nik_ktp,
          'nik' : this.nik,
          'fullname' : this.fullname,
          'companyid' : this.company.companyid,
          'regionalid' : this.regional.regionalid,
          'divisionid' : this.division.divisionid,
          'mainjobid' : this.mainjob.mainjobid,
          'positionid' : this.position.positionid,
          'levelid' : this.level.levelid,
          'educationid' : this.education.eduid,
          'joindate' : this.formatDate(this.joindate),
          'resindate' : null,
          'joincallsign' : this.formatDate(this.joindate),
          'releasecallsign' : null,
          'address' : this.address,
          'email' : this.email,
          'phonenum' : this.phonenum,
          'remark' : this.remark,
          'remark_resign' : null,
          'remark_promote' : null,
          'remark_movement' : null,
          'remark_inactive' : null,
          'statusid' : 2,
          'createdby' : localStorage.getItem('user-username'),
          'replacementemployeeid': this.replacementemployeeid
        }
        this.$axios.post('/employee/replacementemployee', body, {
          headers: {
            Authorization : localStorage.getItem('user-token')
          }
        })
        .then(respone => {
          var data = respone.data
          if (data.fullname != null) {
            if(data.datecreated == null) {
              this.$q.loading.hide()
              this.$q.notify({
                message: data.fullname,
                color: 'red',
                icon: 'warning'
              })
            } else {
              this.$q.loading.hide()
              this.$q.notify({
                message: 'New Employee Created',
                color: 'secondary',
                icon: 'check_circle'
              })
              this.iconreplacement = false
              this.getCompanyList()
              this.getEmployees()
            }
          } else {
            this.$q.loading.hide()
            this.$q.notify({
              message: 'Failed to Create New Employee',
              color: 'red',
              icon: 'warning'
            })
          }
        })
        .catch(error => {
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Error : ' + error,
            color: 'red',
            icon: 'warning'
          })
          console.log('message : ' + error)
          localStorage.removeItem('checknikktp')
        })
      } else {
        this.$q.loading.hide()
        this.$q.notify({
          message: 'Please Complete the Form',
          color: 'red',
          icon: 'warning'
        })
      }
    },
    formatDate (date) {
      var formatDate = date.replace(/\//g, '-')
      var datearray = formatDate.split('-')
      var newdate = datearray[2] + '-' + datearray[1] + '-' + datearray[0]
      return newdate
    },
    detailsData(data){
      this.detail.callsign = data.callsign
      this.detail.nik_ktp = data.nikktp
      this.detail.nik = data.nik
      this.detail.fullname = data.fullname
      this.detail.education = data.education
      this.detail.address = data.address
      this.detail.joindate = data.joindate
      this.detail.joincallsign = data.joincallsign
      this.detail.phonenum = data.phonenum
      this.detail.regional = data.regional
      this.detail.division = data.division
      this.detail.mainjob = data.mainjob
      this.detail.position = data.position
      this.detail.level = data.level
      this.detail.joindate = data.joindate
      this.detail.company = data.company
      this.detail.email = data.email
      this.detail.remark = data.remark
    },
    downloadReport(){
      this.$q.loading.show()
      var start = ''
      var end = ''
      if (this.selectCompany != null) {
        var selected = this.selectCompany.companyname
      } else {
        var selected = ''
      }
      if (this.startdate != ''){
        start = this.formatDate(this.startdate)
      }
      if (this.enddate != ''){
        end = this.formatDate(this.enddate)
      }
      var body = {
        'company': selected,
        'regional': null,
        'division': null,
        'position': null,
        'startdate': start,
        'enddate' : end
      }
      this.$axios.post('/master/getemployeeresignreport', body, {
        responseType: 'blob',
        headers:{
          Authorization: localStorage.getItem('user-token')
        }
      })
      .then(response => {
        const url = window.URL.createObjectURL(new Blob([response.data]))
        const link = document.createElement('a')
        link.href = url
        var fileName = 'Employee Resign Report.xlsx'
        link.setAttribute('download', fileName)
        document.body.appendChild(link)
        link.click()
        this.$q.loading.hide()
      })
      .catch(error => {
        this.$q.loading.hide()
        this.$q.notify({
          message: error,
          color: 'red',
          icon: 'warning'
        })
      })
    }
  },
  beforeMount () {
    this.getEmployees()
    this.getCompanyList()
    this.getDivisionList()
    this.getPositionList()
    this.getRegionalList()
    this.getMainjobList()
    this.getLevelList()
    this.getEducationList()
  }
}