const productivityColumns = [
  { name: 'regionname', align: 'center', label: 'REGION NAME', field: 'regionname', sortable: true, align: 'center' },
  { name: 'callsign', label: 'TEAM', field: 'callsign', sortable: true, align: 'center' },
  { name: 'attedancepoint', label: '% ATTEDANCE', field: 'attedancepoint', align: 'center' },
  { name: 'ticketpoint', label: '% TICKET POINT', field: 'ticketpoint', align: 'center' },
  { name: 'dmobupoint', label: '% DMOBU POINT', field: 'dmobupoint', align: 'center' },
  { name: 'result', label: 'RESULT', field: 'result', align: 'center' }
]

const attedanceColumns = [
  { name: 'regionname', align: 'center', label: 'REGION NAME', field: 'regionname', sortable: true, align: 'center' },
  { name: 'callsign', label: 'TEAM', field: 'callsign', sortable: true, align: 'center' },
  { name: 'totalWorkDays', label: 'TOTAL SCHEDULE', field: 'totalWorkDays', align: 'center' },
  { name: 'totalIn', label: 'COUNT OF IN', field: 'totalIn', align: 'center' },
  { name: 'totalLate', label: 'COUNT OF LATE', field: 'totalLate', align: 'center' },
  { name: 'totalOverdue', label: 'COUNT OF OVERDUE', field: 'totalOverdue', align: 'center' },
  { name: 'result', label: 'ATTEDANCE POINT', field: 'result', align: 'center' }
]


const ticketPointColumns = [
  { name: 'regionname', align: 'center', label: 'REGIONAL NAME', field: 'regionname', sortable: true, align: 'center' },
  { name: 'callsign', label: 'TEAM', field: 'callsign', sortable: true, align: 'center' },
  { name: 'typeA1', label: 'A1', field: 'typeA1', align: 'center' },
  { name: 'typeA2', label: 'A2', field: 'typeA2', align: 'center' },
  { name: 'typeA3', label: 'A3', field: 'typeA3', align: 'center' },
  { name: 'typeB1', label: 'B1', field: 'typeB1', align: 'center' },
  { name: 'typeB2', label: 'B2', field: 'typeB2', align: 'center' },
  { name: 'typeB3', label: 'B3', field: 'typeB3', align: 'center' },
  { name: 'typeMaintenaceNCUP', label: 'Maintenence NCUP', field: 'typeMaintenaceNCUP', align: 'center' },
  { name: 'typeMaintenanceSB', label: 'Maintenence SB', field: 'typeMaintenanceSB', align: 'center' },
  { name: 'typeNF', label: 'NF', field: 'typeNF', align: 'center' },
  { name: 'typeREQ', label: 'REQ', field: 'typeREQ', align: 'center' },
  { name: 'totalTicketPoint', label: 'TOTAL TICKET POINT', field: 'totalTicketPoint', align: 'center' }
]

export default {
  data () {
    return {
      filter : '',
      filterproductivity : '',
      filterattedance : '',
      filterticketpoint : '',
      loadingProductivity : false,
      loadingAttedance : false,
      loadingTicketPoint : false,
      productivityDatas : [],
      attedanceDatas : [],
      ticketPointDatas : [],
      paginationAtt : {
        page: 1,
        rowsPerPage: 6,
        rowsNumber: 0
      },
      paginationPrd : {
        page: 1,
        rowsPerPage: 6,
        rowsNumber: 0
      },
      paginationTp : {
        page: 1,
        rowsPerPage: 6,
        rowsNumber: 0
      },
      periodOptions : [],
      periodSelected : '',
      companyOptions : [],
      companySelected : null,
      startdate: '',
      enddate: '',
      selectPosition: null,
      filter: '',
      tab: 'peformance',
      productivityColumns,
      
      attedanceColumns,
      
      ticketPointColumns,
      
      resignColumns: [],
      resignRows : [],
      positionOptions: []
    }
  },
  methods: {
    getColumns(){ 
      if (this.startdate != ''){
        var start = this.formatDate(this.startdate)
      } else {
        var start = ''
      }

      if (this.enddate != ''){
        var end = this.formatDate(this.enddate)
      } else {
        var end = ''
      }
      this.$axios.get('/report/getcolumns', {
        headers: {
          Authorization: localStorage.getItem('user-token')
        },
        params: {
          'start' : start,
          'end' : end
        }
      }).
      then(response => {
        var data = response.data
        this.resignColumns = data
      })
      .catch(error => {
        console.log(error)
      })
    },
    getDatas(){
      this.$q.loading.show()
      if (this.startdate != ''){
        var start = this.formatDate(this.startdate)
      } else {
        var start = ''
      }

      if (this.enddate != ''){
        var end = this.formatDate(this.enddate)
      } else {
        var end = ''
      }

      if (this.selectPosition != null){
        var post = this.selectPosition.positionid
      } else {
        var post = 0
      }

      this.$axios.get('/report/resigndata', {
        headers: {
          Authorization: localStorage.getItem('user-token')
        },
        params: {
          'positionid' : post,
          'start' : start,
          'end' : end
        }
      })
      .then(response => {
        var data = response.data
        this.resignRows = data
        this.$q.loading.hide()
      })
      .catch(error => {
        console.log(error)
      })
    },
    getPosition(){
      this.$axios.get('/master/getallposition', {
        headers: {
          Authorization: localStorage.getItem('user-token')
        }
      })
      .then(response => {
        this.positionOptions = response.data
      })
      .catch(error => {
        console.log(error)
      })
    },
    filterSearch(){
      this.$q.loading.show()
      if (this.startdate != '' && this.enddate != ''){
        if (this.oneYear(this.startdate, this.enddate) === true) {
          this.$q.loading.hide()
          this.getColumns()
          this.getDatas()
        } else {
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Date Periode Must be in Same Year',
            color: 'red',
            icon: 'warning'
          })
        }
      } else {
        this.$q.loading.hide()
        this.$q.notify({
          message: 'Please Complete Date Periode',
          color: 'red',
          icon: 'warning'
        })
      }
    },
    resetFilter(){
      this.startdate = ''
      this.enddate = ''
      this.selectPosition = null
      this.getColumns()
      this.getDatas()
    },
    formatDate (date) {
      var formatDate = date.replace(/\//g, '-')
      var datearray = formatDate.split('-')
      var newdate = datearray[2] + '-' + datearray[1] + '-' + datearray[0]
      return newdate
    },
    oneYear (start, end) {
      var datearray1 = start.split('/')
      var datearray2 = end.split('/')
      if (datearray1[0] === datearray2[0]) {
        return true
      } else {
        return false
      }
    },
    downloadReport(){
      this.$q.loading.show()
      if (this.startdate != ''){
        var start = this.formatDate(this.startdate)
      } else {
        var start = ''
      }

      if (this.enddate != ''){
        var end = this.formatDate(this.enddate)
      } else {
        var end = ''
      }

      if (this.selectPosition != null){
        var post = this.selectPosition.positionid
      } else {
        var post = 0
      }
      var body = {
        'positionid' : post,
        'startdate' : start,
        'enddate' : end
      }
      this.$axios.post('/report/getresignreport', body, {
        responseType: 'blob',
        headers:{
          Authorization: localStorage.getItem('user-token')
        }
      })
      .then(response => {
        const url = window.URL.createObjectURL(new Blob([response.data]))
        const link = document.createElement('a')
        link.href = url
        var fileName = 'Report Resign.xlsx'
        link.setAttribute('download', fileName)
        document.body.appendChild(link)
        link.click()
        this.$q.loading.hide()
      })
      .catch(error => {
        this.$q.loading.hide()
        this.$q.notify({
          message: error,
          color: 'red',
          icon: 'warning'
        })
      })
    },
    fetchAllPeriod() {
      this.$axios.get('/report/getallperiods', {
        headers:{
          Authorization : localStorage.getItem('user-token')
        }
      })
      .then( response => {
        var data = response.data
        this.periodOptions = data
      })
      .catch ( error => {
        console.log(error)
      })
    },
    fetchAllCompany() {
      this.$axios.get('/master/getcompany', {
        headers :{
          Authorization : localStorage.getItem('user-token')
        }
      })
      .then( response => {
        var data = response.data
        this.companyOptions = data
      })
      .catch ( error => {
        console.log(error)
      })
    },
    fetchAttedanceDatas(cp = 1, rpp = 6, month = this.periodSelected, cc = this.companySelected, fltr = this.filter) {
      this.loadingAttedance = true
      var code = ''
      if (cc == null){
        code = ''
      } else {
        code = cc.companycode
      }
      this.$axios.get('/report/getattedancedata', {
        headers: {
          Authorization : localStorage.getItem('user-token')
        },
        params : {
          periode: month,
          companycode : code,
          rowsperpage : rpp/2,
          currentpage : cp,
          filter : fltr
        }
      })
      .then ( response => {
        var data = response.data
        this.attedanceDatas = data.datas
        
        this.paginationAtt.page = data.currentPage
        this.paginationAtt.rowsPerPage = data.rowsPerPage
        this.paginationAtt.rowsNumber = data.totalRows

        this.loadingAttedance = false
      })
      .catch (error => {
        console.log(error)
        this.loadingAttedance = false
      })
    },
    fetchProductivityDatas(cp = 1, rpp = 6, month = this.periodSelected, cc = this.companySelected, fltr = this.filter) {
      this.loadingProductivity = true
      var code = ''
      if (cc == null){
        code = ''
      } else {
        code = cc.companycode
      }
      this.$axios.get('/report/getproductivitydata', {
        headers: {
          Authorization : localStorage.getItem('user-token')
        },
        params : {
          periode : month,
          companycode : code,
          rowsperpage : rpp/2,
          currentpage : cp,
          filter : fltr
        }
      })
      .then ( response => {
        var data = response.data
        this.productivityDatas = data.datas
        
        this.paginationPrd.page = data.currentPage
        this.paginationPrd.rowsPerPage = data.rowsPerPage
        this.paginationPrd.rowsNumber = data.totalRows

        this.loadingProductivity = false
      })
      .catch (error => {
        console.log(error)
        this.loadingProductivity = false
      })
    },
    fetchTicketPointDatas(cp = 1, rpp = 6, month = this.periodSelected, cc = this.companySelected, fltr = this.filter) {
      this.loadingTicketPoint = true
      var code = ''
      if (cc == null){
        code = ''
      } else {
        code = cc.companycode
      }
      this.$axios.get('/report/getticketpointdata', {
        headers: {
          Authorization : localStorage.getItem('user-token')
        },
        params : {
          periode : month,
          companycode : code,
          rowsperpage : rpp/2,
          currentpage : cp,
          filter : fltr
        }
      })
      .then ( response => {
        var data = response.data
        this.ticketPointDatas = data.datas
        
        this.paginationTp.page = data.currentPage
        this.paginationTp.rowsPerPage = data.rowsPerPage
        this.paginationTp.rowsNumber = data.totalRows

        this.loadingTicketPoint = false
      })
      .catch (error => {
        console.log(error)
        this.loadingTicketPoint = false
      })
    },
    onRequestPrd(props) {
      this.fetchProductivityDatas(props.pagination.page, props.pagination.rowsPerPage, this.periodSelected, this.companySelected, this.filter)
    },
    onRequestAtt(props) {
      this.fetchAttedanceDatas(props.pagination.page, props.pagination.rowsPerPage, this.periodSelected, this.companySelected, this.filter)
    },
    onRequestTp(props) {
      this.fetchTicketPointDatas(props.pagination.page, props.pagination.rowsPerPage, this.periodSelected, this.companySelected, this.filter)
    },
    filterPeformanceSearch(){
      if(filter.value != '' && periodSelected.value == '' && companySelected.value == null){
        this.fetchProductivityDatas(1,2,periodSelected, companySelected, filter)
        this.fetchAttedanceDatas(1,2,periodSelected, companySelected, filter)
        this.fetchTicketPointDatas(1,2,periodSelected, companySelected, filter)
      } else {
        this.fetchProductivityDatas(1,6,periodSelected, companySelected, filter)
        this.fetchAttedanceDatas(1,6,periodSelected, companySelected, filter)
        this.fetchTicketPointDatas(1,6,periodSelected, companySelected, filter)
      }
    },
    resetPeformanceFilter() {
      this.periodSelected = ''
      this.companySelected = null
      this.filter = ''
      this.fetchProductivityDatas()
      this.fetchAttedanceDatas()
      this.fetchTicketPointDatas()
    }
  },
  beforeMount () {
    this.getPosition()
    this.getColumns()
    this.getDatas()
    this.getPosition()
    this.fetchAllPeriod()
    this.fetchAllCompany()
    this.fetchProductivityDatas()
    this.fetchAttedanceDatas()
    this.fetchTicketPointDatas()
  }
}