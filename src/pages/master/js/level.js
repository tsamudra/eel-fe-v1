const columnslevel = [
    {
      name: 'no',
      required: true,
      label: 'No',
      align: 'center',
      sortable: true
    },
    { name: 'skill', align: 'center', label: 'Skill', field: 'skill', sortable: true },
    { name: 'level', label: 'Level', field: 'level', sortable: true, align: 'center' },
    { name: 'levellistid', label: 'Action', field: 'levellistid', align: 'center' }
  ]
export default {
  data () {
    return {
      checkRole: localStorage.getItem('user-role'),
      filterlevel: '',
      iconlevel: false,
      iconlevelnew: false,
      iconskillnew: false,
      iconview: false,
      icondelete: false,
      skill: null,
      level: null,
      levelnew: '',
      columnslevel,
      rowslevel: [],
      positionOptions: [],
      levelOptions: [],
      skillOptions: [],
      skillnew : '',
      del: {
        levellistid: '',
        recordlabel: ''
      },
      upd : {
        levellistid: '',
        skill: null,
        level: null
      }
    }
  },
  methods: {
    checkRoleUser(){
        if(this.checkRole != 'ADMIN'){
            window.location.href = "/#/";
        }
    },
    getLevelList(){
      this.$axios.get('/master/getalllevellist', {
        headers: {
          Authorization: localStorage.getItem('user-token')
        }
      })
      .then(response => {
        var data = response.data
        var i = 0
        for(var key in data){
          data[i]['no'] = i + 1
          i++
        }
        this.rowslevel = data
      })
      .catch(error => {
        console.log('message : '+error)
      })
    },
    getLevelOptions(){
      this.$axios.get('/master/getalllevel', {
        headers: {
          Authorization: localStorage.getItem('user-token')
        }
      })
      .then(response => {
        var data = response.data
        this.levelOptions = data
      })
      .catch(error => {
        console.log('message : '+error)
      })
    },
    getSkillOptions(){
      this.$axios.get('/master/getallskill', {
        headers: {
          Authorization: localStorage.getItem('user-token')
        }
      })
      .then(response => {
        var data = response.data
        this.skillOptions = data
      })
      .catch(error => {
        console.log('message : '+error)
      })
    },
    onSubmit () {
      this.$q.loading.show()
      if (this.skill != null && this.level != null){
        var body = {
          'levellistid' : null,
          'skillname' : this.skill.skillname,
          'levelid': this.level.levelid
        }
        this.$axios.post('/master/createlevellist', body, {
          headers: {
            Authorization: localStorage.getItem('user-token')
          }
        })
        .then(response => {
          var data = response.data
          if (data.levellistid != null) {
            if (data.levellistid == 0) {
              this.$q.loading.hide()
              this.$q.notify({
                message: 'Record Registered. Cannot Save Duplicate Data',
                color: 'red',
                icon: 'warning'
              })
            } else {
              this.$q.loading.hide()
              this.$q.notify({
                message: 'New Record Created',
                color: 'secondary',
                icon: 'check_circle'
              })
              this.iconlevel = false
              this.getLevelList()
              this.onReset()
            }
          } else {
            this.$q.loading.hide()
            this.$q.notify({
              message: 'Failed to Create New Record',
              color: 'red',
              icon: 'warning'
            })
            this.iconlevel = false
            this.getLevelList()
          }
        })
        .catch(error => {
          this.$q.loading.hide()
          this.$q.notify({
            message: error,
            color: 'red',
            icon: 'warning'
          })
          this.iconlevel = false
          this.getLevelList()
        })
      } else {
        this.$q.loading.hide()
        this.$q.notify({
          message: 'Please Complete the Form',
          color: 'red',
          icon: 'warning'
        })
      }
    },
    onReset () {
      this.skill = null
      this.level = null
    },
    onSubmitLevel () {
      this.$q.loading.show()
      if (this.levelnew != ''){
        var body = {
          'levelid' : null,
          'levelname' : this.levelnew
        }
        this.$axios.post('/master/createlevel', body, {
          headers: {
            Authorization: localStorage.getItem('user-token')
          }
        })
        .then(response => {
          var data = response.data
          console.log(data)
          if (data.levelname != null){
            if (data.datecreated != null){
              this.$q.loading.hide()
              this.$q.notify({
                message: 'New Level Created',
                color: 'secondary',
                icon: 'check_circle'
              })
              this.iconlevelnew = false
              this.getLevelOptions()
              this.onResetLevel()
            } else {
              this.$q.loading.hide()
              this.$q.notify({
                message: data.levelname,
                color: 'red',
                icon: 'warning'
              })
            }
          } else {
            this.$q.loading.hide()
            this.$q.notify({
              message: 'Failed to Create New Level',
              color: 'red',
              icon: 'warning'
            })
            this.iconlevelnew = false
          }
        })
        .catch(error => {
          this.$q.loading.hide()
          this.$q.notify({
            message: error,
            color: 'red',
            icon: 'warning'
          })
          this.iconlevelnew = false
        })
      } else {
        this.$q.loading.hide()
        this.$q.notify({
          message: 'Please Complete the Form',
          color: 'red',
          icon: 'warning'
        })
      }
    },
    onResetLevel () {
      this.levelnew = null
    },
    doUpdate (id) {
      this.$q.loading.show()
      this.$axios.get('/master/getlevellistbyid', {
        headers: {
          Authorization: localStorage.getItem('user-token')
        },
        params: {
          'id' : id
        }
      })
      .then(response => {
        var data = response.data
        this.upd.levellistid = data.levellistid
        this.upd.skill = data.skill
        this.upd.level = data.level
        this.$q.loading.hide()
        this.iconview = true
      })
      .catch(error => {
        this.$q.loading.hide()
        this.$q.notify({
          message: error,
          color: 'red',
          icon: 'warning'
        })
      })
    },
    doDelete (id) {
      this.$q.loading.show()
      this.$axios.get('/master/getlevellistbyid', {
        headers: {
          Authorization: localStorage.getItem('user-token')
        },
        params: {
          'id' : id
        }
      })
      .then(response => {
        var data = response.data
        this.del.levellistid = data.levellistid
        this.del.recordlabel = data.position.positionname  + ' - ' + data.level.levelname
        this.$q.loading.hide()
        this.icondelete = true
      })
      .catch(error => {
        this.$q.loading.hide()
        this.$q.notify({
          message: error,
          color: 'red',
          icon: 'warning'
        })
      })
    },
    deletePosition(id) {
      this.$q.loading.show()
      this.$axios.delete('/master/deletelevellist', {
        headers: {
          Authorization: localStorage.getItem('user-token')
        },
        params: {
          'id' : id
        }
      })
      .then(response => {
        if (response.data == 1) {
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Record Deleted',
            color: 'secondary',
            icon: 'check_circle'
          })
          this.icondelete = false
          this.getLevelList()
        } else {
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Failed to Delete Record',
            color: 'red',
            icon: 'warning'
          })
          this.icondelete = false
          this.getLevelList()
        }
      })
      .catch(error => {
        this.$q.loading.hide()
        this.$q.notify({
          message: error,
          color: 'red',
          icon: 'warning'
        })
        this.icondelete = false
          this.getLevelList()
      })
    },
    updateRecord(){
      this.$q.loading.show()
      if (this.upd.skill != null && this.upd.level != null){
        var body = {
          'levellistid' : this.upd.levellistid,
          'skillname' : this.upd.skill.skillname,
          'levelid' : this.upd.level.levelid
        }
        this.$axios.patch('/master/updatelevellist', body, {
          headers: {
            Authorization: localStorage.getItem('user-token')
          }
        })
        .then(response => {
          var data = response.data
          if ( data.levellistid != null){
            if (data.levellistid == 0) {
              this.$q.loading.hide()
              this.$q.notify({
                message: 'Record Registered. Cannot Save Duplicate Data',
                color: 'red',
                icon: 'warning'
              })
            } else {
              this.$q.loading.hide()
              this.$q.notify({
                message: 'Record Updated',
                color: 'secondary',
                icon: 'check_circle'
              })
              this.iconview = false
              this.getLevelList()
            }
          } else {
            this.$q.loading.hide()
            this.$q.notify({
              message: 'Failed to Update Record',
              color: 'red',
              icon: 'warning'
            })
            this.iconview = false
            this.getLevelList()
          }
        })
        .catch(error => {
          this.$q.loading.hide()
          this.$q.notify({
            message: error,
            color: 'red',
            icon: 'warning'
          })
          this.iconview = false
          this.getLevelList()
        })
      } else {
        this.$q.loading.hide()
        this.$q.notify({
          message: 'Please Complete the Form',
          color: 'red',
          icon: 'warning'
        })
      }
    },
    onSubmitSkill(){
      this.$q.loading.show()
      if (this.skillnew != ''){
        var body = {
          'skillid' : null,
          'skillname' : this.skillnew
        }
        this.$axios.post('/master/createskill', body, {
          headers: {
            Authorization: localStorage.getItem('user-token')
          }
        })
        .then(response => {
          var data = response.data
          if (data.datecreated != null){
            this.$q.loading.hide()
            this.$q.notify({
              message: 'New Skill Created',
              color: 'secondary',
              icon: 'check_circle'
            })
            this.iconskillnew = false
            this.getSkillOptions()
            this.onResetSkill()
          } else {
            this.$q.loading.hide()
            this.$q.notify({
              message: 'Failed Crete New Skill',
              color: 'red',
              icon: 'warning'
            })
            this.iconskillnew = false
          }
        })
        .catch(error => {
          this.$q.loading.hide()
          this.$q.notify({
            message: error,
            color: 'red',
            icon: 'warning'
          })
          this.iconlevelnew = false
        })
      } else {
        this.$q.loading.hide()
        this.$q.notify({
          message: 'Please Complete the Form',
          color: 'red',
          icon: 'warning'
        })
      }
    },
    onResetSkill(){
      this.skillnew = ''
    },
  },
  beforeMount () {
    this.checkRoleUser()
    this.getLevelList()
    this.getLevelOptions()
    this.getSkillOptions()
  }
}