const bonusColumns = [
    { name: 'no', align: 'center', label: 'No', field: 'no', sortable: true, align: 'center' },
    { name: 'tickettype', label: 'Ticket Type', field: 'tickettype', sortable: true, align: 'center' },
    { name: 'point1', label: 'Node Age < 10 Years', field: 'point1', align: 'center' },
    { name: 'point2', label: '10 Years < Node Age > 20 Years', field: 'point2', align: 'center' },
    { name: 'point3', label: '20 Years < Node Age', field: 'point3', align: 'center' },
    { name: 'multiple', label: 'Multiple', field: 'multiple', align: 'center' },
    { name: 'multiplepoint', label: 'Multiple Point', field: 'multiplepoint', align: 'center' },
    { name: 'note', label: 'Note', field: 'note', align: 'center' },
    { name: 'bonusid', label: 'Action', field: 'bonusid', align: 'center' }
]

const skillSetColumns = [
    { name: 'no', align: 'center', label: 'No', field: 'no', sortable: true, align: 'center' },
    { name: 'grouptype', label: 'Title', field: 'grouptype', sortable: true, align: 'center' },
    { name: 'level1', label: 'Leveling 1', field: 'level1', align: 'center' },
    { name: 'level2', label: 'Leveling 2', field: 'level2', align: 'center' },
    { name: 'code', label: 'Code', field: 'code', align: 'center' },
    { name: 'skillsetid', label: 'Action', field: 'skillsetid', align: 'center' }
]

export default {
    data () {
        return {
            checkRole: localStorage.getItem('user-role'),
            tab: 'bonus',
            filterpremium: '',
            filteradvance: '',
            filterreguler: '',
            filterskillset: '',
            iconcreate: false,
            iconupdate: false,
            icondelete: false,
            iconcreateskillset: false,
            iconupdateskillset: false,
            icondeleteskillset: false,
            bonusColumns,
            skillSetColumns,
            bonusPremiumRows : [],
            bonusAdvanceRows : [],
            bonusRegulerRows : [],
            skillSetRows: [],
            levelOptions: [],
            grouptypeOptions: [
                'PREMIUM', 'ADVANCED', 'REGULER'
            ],
            multipleOptions: [
                "YES", "NO"
            ],
            newBonus : {
                grouptype: '',
                tickettype: '',
                point1: '',
                point2: '',
                point3: '',
                multiple: '',
                multiplepoint: null,
                note: null
            },
            upd : {
                bonusid: '',
                grouptype: '',
                tickettype: '',
                point1: '',
                point2: '',
                point3: '',
                multiple: '',
                multiplepoint: '',
                note: ''
            },
            del: {
                grouptype: '',
                bonusid : '',
                recordbonus : ''
            },
            newSkillSet : {
                grouptype : '',
                level1 : '',
                level2 : '',
                code : '',
            },
            updSkillSet : {
                skillsetid : '',
                grouptype : '',
                level1 : '',
                level2 : '',
                code : '',
            },
            delSkillSet : {
                skillsetid: '',
                recordskillset: ''
            }
        }
    },
    methods: {
        checkRoleUser(){
            if(this.checkRole != 'ADMIN'){
                window.location.href = "/#/";
            }
        },
        getLevelOptions(){
            this.$axios.get('/master/getalllevel', {
                headers: {
                    Authorization : localStorage.getItem('user-token')
                }
            })
            .then(response => {
                this.levelOptions = response.data
            })
            .catch(error => {
                console.log(error)
            })
        },
        getSkillSetRows(){
            this.$q.loading.show()
            this.$axios.get('/master/getallskillset', {
                headers: {
                    Authorization: localStorage.getItem('user-token')
                }
            })
            .then(response => {
                var data = response.data
                var i = 0
                for (var key in data){
                data[i]['no'] = i + 1
                i++
                }
                this.skillSetRows = data
                this.$q.loading.hide()
            })
            .catch(error => {
                console.log(error)
                this.$q.loading.hide()
            })
        },
        getPremiumRows(){
            this.$q.loading.show()
            this.$axios.get('/master/getallbonus', {
                headers: {
                    Authorization: localStorage.getItem('user-token')
                },
                params: {
                    'grouptype' : 'PREMIUM'
                }
            })
            .then(response => {
                var data = response.data
                var i = 0
                for (var key in data){
                data[i]['no'] = i + 1
                i++
                }
                this.bonusPremiumRows = data
                this.$q.loading.hide()
            })
            .catch(error => {
                console.log(error)
                this.$q.loading.hide()
            })
        },
        getAdvancedRows(){
            this.$q.loading.show()
            this.$axios.get('/master/getallbonus', {
                headers: {
                    Authorization: localStorage.getItem('user-token')
                },
                params: {
                    'grouptype' : 'ADVANCED'
                }
            })
            .then(response => {
                var data = response.data
                var i = 0
                for (var key in data){
                data[i]['no'] = i + 1
                i++
                }
                this.bonusAdvanceRows = data
                this.$q.loading.hide()
            })
            .catch(error => {
                console.log(error)
                this.$q.loading.hide()
            })
        },
        getRegulerRows(){
            this.$q.loading.show()
            this.$axios.get('/master/getallbonus', {
                headers: {
                    Authorization: localStorage.getItem('user-token')
                },
                params: {
                    'grouptype' : 'REGULER'
                }
            })
            .then(response => {
                var data = response.data
                var i = 0
                for (var key in data){
                data[i]['no'] = i + 1
                i++
                }
                this.bonusRegulerRows = data
                this.$q.loading.hide()
            })
            .catch(error => {
                console.log(error)
                this.$q.loading.hide()
            })
        },
        onSubmitBonus(){
            this.$q.loading.show()
            if ( this.newBonus.grouptype != '' && this.newBonus.tickettype != '' && this.newBonus.point1 != '' && this.newBonus.point2 != '' && this.newBonus.point3 != '' && this.newBonus.multiple != '' && this.newBonus.multiplepoint != null && this.newBonus.multiplepoint != '' ){
                if (this.newBonus.multiplepoint == 0){
                    this.newBonus.multiplepoint = null
                }
                var body = {
                    'bonusid' : null,
                    'grouptype' : this.newBonus.grouptype,
                    'tickettype' : this.newBonus.tickettype,
                    'point1' : this.newBonus.point1,
                    'point2' : this.newBonus.point2,
                    'point3' : this.newBonus.point3,
                    'multiple' : this.newBonus.multiple,
                    'multiplepoint' : this.newBonus.multiplepoint,
                    'note' : this.newBonus.note
                }
                this.$axios.post('/master/createbonus', body, {
                    headers: {
                        Authorization : localStorage.getItem('user-token')
                    }
                })
                .then(response => {
                    var res = response.data
                    if (res.datecreated != null){
                        this.$q.notify({
                            message: 'New Bonus Created',
                            color: 'secondary',
                            icon: 'check_circle'
                        })
                        if (this.newBonus.grouptype == 'PREMIUM'){
                            this.getPremiumRows()
                        } else if (this.newBonus.grouptype == 'ADVANCED'){
                            this.getAdvancedRows()
                        } else if (this.newBonus.grouptype == 'REGULER'){
                            this.getRegulerRows()
                        }
                        this.iconcreate = false
                        this.onResetBonus()
                        this.$q.loading.hide()
                    } else {
                        this.$q.notify({
                            message: res.note,
                            color: 'red',
                            icon: 'warning'
                        })
                        this.$q.loading.hide()
                    }
                })
                .catch(error => {
                    console.log(this.newBonus)
                    this.$q.notify({
                        message: error,
                        color: 'red',
                        icon: 'warning'
                    })
                    this.$q.loading.hide()
                })
            } else {
                this.$q.notify({
                    message: 'Please Complete the Form',
                    color : 'red',
                    icon: 'warning'
                })
                this.$q.loading.hide()
            }
        }, 
        onResetBonus() {
            this.newBonus.grouptype = '',
            this.newBonus.tickettype = '',
            this.newBonus.point1 = '',
            this.newBonus.point2 = '',
            this.newBonus.point3 = '',
            this.newBonus.multiple = '',
            this.newBonus.multiplepoint = null,
            this.newBonus.note = null
        },
        onUpdateBonus(){
            this.$q.loading.show()
            console.log(this.upd.grouptype)
            console.log(this.upd.tickettype)
            console.log(this.upd.point1)
            console.log(this.upd.point2)
            console.log(this.upd.point3)
            console.log(this.upd.multiple)
            console.log(this.upd.multiplepoint)
            console.log(this.upd.note)
            if ( this.upd.grouptype != '' && this.upd.tickettype != '' && this.upd.point1 != '' && this.upd.point2 != '' && this.upd.point3 != '' && this.upd.multiple != ''){
                if (this.upd.multiplepoint == 0){
                    this.upd.multiplepoint = null
                }
                var body = {
                    'bonusid' :this.upd.bonusid,
                    'grouptype' : this.upd.grouptype,
                    'tickettype' : this.upd.tickettype,
                    'point1' : this.upd.point1,
                    'point2' : this.upd.point2,
                    'point3' : this.upd.point3,
                    'multiple' : this.upd.multiple,
                    'multiplepoint' : this.upd.multiplepoint,
                    'note' : this.upd.note
                }
                this.$axios.patch('/master/updatebonus', body, {
                    headers: {
                        Authorization : localStorage.getItem('user-token')
                    }
                })
                .then(response => {
                    var res = response.data
                    console.log(res)
                    if (res.datecreated != null){
                        this.$q.notify({
                            message: 'New Bonus Created',
                            color: 'secondary',
                            icon: 'check_circle'
                        })
                        if (this.upd.grouptype == 'PREMIUM'){
                            this.getPremiumRows()
                        } else if (this.upd.grouptype == 'ADVANCED'){
                            this.getAdvancedRows()
                        } else if (this.upd.grouptype == 'REGULER'){
                            this.getRegulerRows()
                        }
                        this.iconupdate = false
                        this.onResetUpdateBonus()
                        this.$q.loading.hide()
                    } else {
                        this.$q.notify({
                            message: res.note,
                            color: 'red',
                            icon: 'warning'
                        })
                        this.$q.loading.hide()
                    }
                })
                .catch(error => {
                    console.log(this.newBonus)
                    this.$q.notify({
                        message: error,
                        color: 'red',
                        icon: 'warning'
                    })
                    this.$q.loading.hide()
                })
            } else {
                this.$q.notify({
                    message: 'Please Complete the Form',
                    color : 'red',
                    icon: 'warning'
                })
                this.$q.loading.hide()
            }
        }, 
        onResetUpdateBonus() {
            this.upd.bonusid = ''
            this.upd.grouptype = '',
            this.upd.tickettype = '',
            this.upd.point1 = '',
            this.upd.point2 = '',
            this.upd.point3 = '',
            this.upd.multiple = '',
            this.upd.multiplepoint = null,
            this.upd.note = null
        },
        doUpdate(id){
            this.$q.loading.show()
            this.$axios.get('/master/getbonusbyid',{
                headers: {
                    Authorization: localStorage.getItem('user-token')
                },
                params: {
                    'bonusid' : id
                }
            })
            .then(response => {
                var data = response.data
                this.upd.bonusid = data.bonusid
                this.upd.grouptype = data.grouptype
                this.upd.tickettype = data.tickettype
                this.upd.point1 = data.point1
                this.upd.point2 = data.point2
                this.upd.point3 = data.point3
                this.upd.multiple = data.multiple
                if (data.multiplepoint == null){
                    this.upd.multiplepoint = 0
                } else {
                    this.upd.multiplepoint = data.multiplepoint
                }
                this.upd.note = data.note
                this.iconupdate = true
                this.$q.loading.hide()
            })
            .catch(error => {
                console.log(error)
                this.$q.notify({
                    message: error,
                    color: 'red',
                    icon: 'warning'
                })
                this.$q.loading.hide()
            })
        },
        doDelete(id){
            this.$q.loading.show()
            this.$axios.get('/master/getbonusbyid',{
                headers: {
                    Authorization: localStorage.getItem('user-token')
                },
                params: {
                    'bonusid' : id
                }
            })
            .then(response => {
                var data = response.data
                this.del.bonusid = data.bonusid
                this.del.recordbonus = data.grouptype + ' - ' + data.tickettype
                this.del.grouptype = data.grouptype
                this.icondelete = true
                this.$q.loading.hide()
            })
            .catch(error => {
                console.log(error)
                this.$q.notify({
                    message: error,
                    color: 'red',
                    icon: 'warning'
                })
                this.$q.loading.hide()
            })
        },
        deleteBonus(id, type){
            this.$q.loading.show()
            this.$axios.delete('/master/deletebonus', {
                headers: {
                    Authorization : localStorage.getItem('user-token')
                },
                params: {
                    'id' : id
                }
            })
            .then(response => {
                if (response.data == 1){
                    this.$q.loading.hide()
                    this.$q.notify({
                        message: 'Record Deleted',
                        color: 'secondary',
                        icon: 'check_circle'
                    })
                    this.icondelete = false
                    if ( type == 'PREMIUM') {
                        this.getPremiumRows()
                    } else if ( type == 'ADVANCED') {
                        this.getAdvancedRows()
                    } else if ( type == 'REGULER'){
                        this.getRegulerRows()
                    }
                } else {
                    this.$q.loading.hide()
                    this.$q.notify({
                        message: 'Failed to Delete',
                        color: 'red',
                        icon: 'warning'
                    })
                }
            })
            .catch(error => {
                this.$q.loading.hide()
                console.log(error)
                this.$q.notify({
                    message: error,
                    color: 'red',
                    icon: 'warning'
                })
            })
        },
        doUpdateSkillSet(id){
            this.$q.loading.show()
            this.$axios.get('/master/getskillsetbyid',{
                headers: {
                    Authorization: localStorage.getItem('user-token')
                },
                params: {
                    'id' : id
                }
            })
            .then(response => {
                var data = response.data
                this.updSkillSet.skillsetid = data.skillsetid
                this.updSkillSet.grouptype = data.grouptype
                this.updSkillSet.level1 = data.level1
                this.updSkillSet.level2 = data.level2
                this.updSkillSet.code = data.code
                this.iconupdateskillset = true
                this.$q.loading.hide()
            })
            .catch(error => {
                console.log(error)
                this.$q.notify({
                    message: error,
                    color: 'red',
                    icon: 'warning'
                })
                this.$q.loading.hide()
            })
        },
        doDeleteSkillSet(id){
            this.$q.loading.show()
            this.$axios.get('/master/getskillsetbyid',{
                headers: {
                    Authorization: localStorage.getItem('user-token')
                },
                params: {
                    'id' : id
                }
            })
            .then(response => {
                var data = response.data
                this.delSkillSet.skillsetid = data.skillsetid
                this.delSkillSet.recordskillset = data.grouptype + ' - (' + data.level1.levelname + ' & ' + data.level2.levelname + ')'
                this.icondeleteskillset = true
                this.$q.loading.hide()
            })
            .catch(error => {
                console.log(error)
                this.$q.notify({
                    message: error,
                    color: 'red',
                    icon: 'warning'
                })
                this.$q.loading.hide()
            })
        },
        deleteSkillSet(id){
            console.log(id)
            this.$q.loading.show()
            this.$axios.delete('/master/deleteskillset', {
                headers: {
                    Authorization : localStorage.getItem('user-token')
                },
                params: {
                    'id' : id
                }
            })
            .then(response => {
                if (response.data == 1){
                    this.$q.loading.hide()
                    this.$q.notify({
                        message: 'Record Deleted',
                        color: 'secondary',
                        icon: 'check_circle'
                    })
                    this.icondeleteskillset = false
                    this.getSkillSetRows()
                } else {
                    this.$q.loading.hide()
                    this.$q.notify({
                        message: 'Failed to Delete',
                        color: 'red',
                        icon: 'warning'
                    })
                }
            })
            .catch(error => {
                this.$q.loading.hide()
                console.log(error)
                this.$q.notify({
                    message: error,
                    color: 'red',
                    icon: 'warning'
                })
            })
        },
        onSubmitSkillSet(){
            this.$q.loading.show()
            if (this.newSkillSet.grouptype != '' && this.newSkillSet.level1 != '' && this.newSkillSet.level2 != '' && this.newSkillSet.code != ''){
                var body = {
                    'skillsetid' : null,
                    'grouptype' : this.newSkillSet.grouptype,
                    'level1' : this.newSkillSet.level1.levelname,
                    'level2' : this.newSkillSet.level2.levelname,
                    'code' : this.newSkillSet.code
                }
                this.$axios.post('/master/createskillset', body, {
                    headers: {
                        Authorization : localStorage.getItem('user-token')
                    }
                })
                .then(response => {
                    var data = response.data
                    if (data.datecreated != null){
                        this.$q.loading.hide()
                        this.$q.notify({
                            message: 'New Skill Set Created',
                            color: 'secondary',
                            icon: 'check_circle'
                        })
                        this.iconcreateskillset = false
                        this.getSkillSetRows()
                        this.onResetSkillSet()
                    } else {
                        this.$q.loading.hide()
                        this.$q.notify({
                            message: data.code,
                            color: 'red',
                            icon: 'warning'
                        })
                    }
                })
                .catch(error => {
                    this.$q.loading.hide()
                    console.log(error)
                    this.$q.notify({
                        message: error,
                        color: 'red',
                        icon: 'warning'
                    })
                })
            } else {
                this.$q.loading.hide()
                this.$q.notify({
                    message: 'Please Complete the Form',
                    color: 'red',
                    icon: 'warning'
                })
            }
        },
        onResetSkillSet(){
            this.newSkillSet.grouptype = ''
            this.newSkillSet.level1 = ''
            this.newSkillSet.level2 = ''
            this.newSkillSet.code = ''
        },
        onUpdateSkillSet(){
            this.$q.loading.show()
            if (this.updSkillSet.grouptype != '' && this.updSkillSet.level1 != '' && this.updSkillSet.level2 != '' && this.updSkillSet.code != ''){
                var body = {
                    'skillsetid' : this.updSkillSet.skillsetid,
                    'grouptype' : this.updSkillSet.grouptype,
                    'level1' : this.updSkillSet.level1.levelname,
                    'level2' : this.updSkillSet.level2.levelname,
                    'code' : this.updSkillSet.code
                }
                this.$axios.patch('/master/updateskillset', body, {
                    headers: {
                        Authorization : localStorage.getItem('user-token')
                    }
                })
                .then(response => {
                    var data = response.data
                    if (data.datecreated != null){
                        this.$q.loading.hide()
                        this.$q.notify({
                            message: 'Skill Set Updated',
                            color: 'secondary',
                            icon: 'check_circle'
                        })
                        this.iconupdateskillset = false
                        this.getSkillSetRows()
                        this.onResetUpdateSkillSet()
                    } else {
                        this.$q.loading.hide()
                        this.$q.notify({
                            message: data.code,
                            color: 'red',
                            icon: 'warning'
                        })
                    }
                })
                .catch(error => {
                    this.$q.loading.hide()
                    console.log(error)
                    this.$q.notify({
                        message: error,
                        color: 'red',
                        icon: 'warning'
                    })
                })
            } else {
                this.$q.loading.hide()
                this.$q.notify({
                    message: 'Please Complete the Form',
                    color: 'red',
                    icon: 'warning'
                })
            }
        },
        onResetUpdateSkillSet(){
            this.updSkillSet.skillsetid = ''
            this.updSkillSet.grouptype = ''
            this.updSkillSet.level1 = ''
            this.updSkillSet.level2 = ''
            this.updSkillSet.code = ''
        },

    },
    beforeMount(){
        this.checkRoleUser()
        this.getLevelOptions()
        this.getSkillSetRows()
        this.getPremiumRows()
        this.getAdvancedRows()
        this.getRegulerRows()
    }
}
