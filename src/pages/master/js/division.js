const columns = [
    {
      name: 'no',
      required: true,
      label: 'No',
      align: 'center',
      sortable: true
    },
    { name: 'divisioname', align: 'center', label: 'Division', field: 'divisioname', sortable: true },
    { name: 'divisionid', label: 'Action', field: 'divisionid', align: 'center' }
  ]
export default {  
    data () {
      return {
        checkRole: localStorage.getItem('user-role'),
        filter: '',
        icon: false,
        icondelete: false,
        iconview: false,
        division: '',
        div: {
          divisionid: '',
          divisioname: ''
        },
        columns,
        rows: []
      }
    },
    methods: {
      checkRoleUser(){
          if(this.checkRole != 'ADMIN'){
              window.location.href = "/#/";
          }
      },
      getDivisionList(){
        this.$axios.get('/master/getalldivision', {
          headers: {
            Authorization: localStorage.getItem('user-token')
          }
        })
        .then(response => {
          var data = response.data
          var i = 0;
          for (var key in data) {
            data[i]['no'] = i + 1
            i++
          }
          this.rows = data
        })
        .catch(error => {
          console.log('message : '+error)
        })
      },
      onSubmit () {
        this.$q.loading.show()
        if (this.division != ''){
          var body = {
            'divisionid' : null,
            'divisionname' : this.division
          }
          this.$axios.post('/master/createdivision', body, {
            headers:{
              Authorization: localStorage.getItem('user-token')
            }
          })
          .then(response => {
            var data = response.data
            if(data.datecreated != null){
              this.icon = false
              this.$q.loading.hide()
              this.$q.notify({
                message: 'New Division Created',
                color: 'secondary',
                icon: 'check_circle'
              })
              this.getDivisionList()
              this.onReset()
            } else {
              this.$q.loading.hide()
              this.$q.notify({
                message: data.divisioname,
                color: 'red',
                icon: 'warning'
              })
            }
          })
        } else {
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Please Complete the Form',
            color: 'red',
            icon: 'warning'
          })
        }
      },
      onReset () {
        this.division = null
      },
      doUpdate (id) {
        this.$axios.get('/master/getdivisionbyid', {
          headers: {
            Authorization: localStorage.getItem('user-token')
          },
          params: {
            'id': id
          }
        })
        .then(response => {
          var data = response.data
          this.div.divisionid = data.divisionid
          this.div.divisioname = data.divisioname
          this.iconview = true
        })
        .catch(error => {
          this.iconview = false
          this.$q.notify({
            message: error,
            color: 'red',
            icon: 'warning'
          })
        })
      },
      doDelete (id) {
        this.$axios.get('/master/getdivisionbyid', {
          headers: {
            Authorization: localStorage.getItem('user-token')
          },
          params: {
            'id': id
          }
        })
        .then(response => {
          var data = response.data
          this.div.divisionid = data.divisionid
          this.div.divisioname = data.divisioname
          this.icondelete = true
        })
        .catch(error => {
          this.icondelete = false
          this.$q.notify({
            message: error,
            color: 'red',
            icon: 'warning'
          })
        })
      },
      deleteDivision(id){
        this.$axios.delete('/master/deletedivision',{
          headers:{
            Authorization: localStorage.getItem('user-token')
          },
          params: {
            'id':id
          }
        })
        .then(response => {
          if(response.data == 1){
            this.icondelete = false
            this.$q.notify({
              message: 'Division Deleted',
              color: 'secondary',
              icon: 'check_circle'
            })
            this.getDivisionList()
          } else {
            this.icondelete = false
            this.$q.notify({
              message: 'Failed to Delete',
              color: 'red',
              icon: 'warning'
            })
            this.getDivisionList()
          }
        })
        .catch(error => {
          this.icondelete = false
          this.getDivisionList()
          this.$q.notify({
            message: error,
            color: 'red',
            icon: 'warning'
          })
        })
      },
      updateDivision(){
        this.$q.loading.show()
        if(this.div.divisioname != ''){
          var body = {
            'divisionid':this.div.divisionid,
            'divisionname':this.div.divisioname
          }
          this.$axios.patch('/master/updatedivision', body,{
            headers: {
              Authorization: localStorage.getItem('user-token')
            }
          })
          .then(response => {
            var data = response.data
            if(data.datecreated != null){
              this.$q.loading.hide()
              this.iconview = false
              this.$q.notify({
                message: 'Division Updated',
                color: 'secondary',
                icon: 'check_circle'
              })
              this.getDivisionList()
            } else {
              this.$q.loading.hide()
              this.$q.notify({
                message: data.divisioname,
                color: 'red',
                icon: 'warning'
              })
            }
          })
          .catch(error =>{
            this.$q.loading.hide()
            this.iconview = false
            this.$q.notify({
              message: error,
              color: 'red',
              icon: 'warning'
            })
          })
        } else {
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Please Complete the Form',
            color: 'red',
            icon: 'warning'
          })
        }
      }
    },
    beforeMount () {
      this.checkRoleUser()
      this.getDivisionList()
    }
  }