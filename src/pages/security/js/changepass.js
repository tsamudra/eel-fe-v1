export default {  
    data () {
      return {
        isPwd: true,
        isPwd2: true,
        user: {
          userid: localStorage.getItem('user-id'),
          roleid: localStorage.getItem('user-role'),
          companyid: localStorage.getItem('user-company'),
          regionalid: localStorage.getItem('user-regional'),
          fullname: localStorage.getItem('user-fullname'),
          username: localStorage.getItem('user-username'),
          password: '',
          passwordconf: ''
        }
      }
    },
    methods: {
      onSubmit () {
        this.$q.loading.show()
        if (this.user.password != '' && this.user.passwordconf != ''){
          if (this.user.password == this.user.passwordconf) {
            var body = {
              'userid' : this.user.userid,
              'roleid' : this.user.roleid,
              'companyid' : this.user.companyid,
              'regionalid' : this.user.regionalid,
              'fullname' : this.user.fullname,
              'username' : this.user.username,
              'password' : this.user.password,
            }
            this.$axios.patch('/master/updateuserpassword', body, {
              headers: {
                Authorization: localStorage.getItem('user-token')
              }
            })
            .then(response => {
              var data = response.data
              if (data.username != null){
                this.$q.loading.hide()
                this.$q.notify({
                  message: 'Account Password Changed',
                  color: 'secondary',
                  icon: 'check_circle'
                })
                this.onReset()
              } else {
                this.$q.loading.hide()
                this.$q.notify({
                  message: 'Failed to Update Password',
                  color: 'red',
                  icon: 'warning'
                })
                this.onReset()
              }
            })
            .catch(error => {
              this.$q.loading.hide()
              this.$q.notify({
                message: error,
                color: 'red',
                icon: 'warning'
              })
              this.onReset()
            })
          } else {
            this.$q.loading.hide()
            this.$q.notify({
              message: 'Password Confirmation Not Match With New Password',
              color: 'red',
              icon: 'warning'
            })
          }
        } else {
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Please Complete the Form',
            color: 'red',
            icon: 'warning'
          })
        }
      },
      onReset () {
        this.user.password = null
        this.user.passwordconf = null
      }
    },
    beforeMount () {

    }
  }