export default {  
  data () {
    return {
      cred: {
        username: '',
        password: ''
      }
    }
  },
  methods: {
    onSubmit () {
      this.$q.loading.show()
      if (this.cred.username != '' && this.cred.password != ''){
        this.$axios.post('auth/login', this.cred)
        .then(response => {
          localStorage.setItem('user-token', 'Bearer ' + response.data.jwt)
          localStorage.setItem('user-role', response.data.rolename)
          localStorage.setItem('user-id', response.data.userid)
          localStorage.setItem('user-company', response.data.companyid)
          localStorage.setItem('user-regional', response.data.regionalid)
          localStorage.setItem('user-username', response.data.username)
          localStorage.setItem('user-fullname', response.data.fullname)
          this.$q.loading.hide()
          this.$router.push('/')
        })
        .catch(error => {
          console.log(error)
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Username / Password Not Match',
            color: 'red '
          })
          
        })
      } else {
        this.$q.loading.hide()
        this.$q.notify({
          message: 'Please Complete the Login Form',
          color: 'red',
          icon: 'warning'
        })
      }
    },
    onReset () {
      this.cred.username = null
      this.cred.password = null
    }
  },
  beforeMount () {
  },
  created () {
    localStorage.removeItem('user-token')
    localStorage.removeItem('user-role')
    localStorage.removeItem('user-id')
    localStorage.removeItem('user-regional')
    localStorage.removeItem('user-company')
    localStorage.removeItem('user-username')
    localStorage.removeItem('user-fullname')
  }
}