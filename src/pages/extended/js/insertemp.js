export default {
    data (){
        return {
          employeeid: '',
          callsign: '',
          nik: '',
          nik_ktp: '',
          fullname: '',
          company: '',
          regional: '',
          division: '',
          mainjob: '',
          position: '',
          joindate: '',
          joincallsign: '',
          releasecallsign: '',
          address: '',
          education: '',
          level: '',
          phonenum: '',
          email: '',
          remark: null,
          optionsCompany: [],
          optionsRegional: [],
          optionsDivision: [],
          optionsPosition: [],
          optionsMainjob: [],
          optionsEducation: [],
          optionsLevel: []
        }
    },
    methods: {
      onSubmit() {        
        this.$q.loading.show()
        if (this.callsign != '' && this.nik_ktp != '' && this.nik != '' && this.fullname != '' && this.address != '' && this.education != '' && this.phonenum != '' && this.regional != '' && this.division != '' && this.mainjob != '' && this.position != '' && this.level != '' && this.joindate != '' && this.company != '' && this.email != ''){
          var body = {
            'employeeid' : null,
            'callsign' : this.callsign,
            'nikktp' : this.nik_ktp,
            'nik' : this.nik,
            'fullname' : this.fullname,
            'companyid' : this.company.companyid,
            'regionalid' : this.regional.regionalid,
            'divisionid' : this.division.divisionid,
            'mainjobid' : this.mainjob.mainjobid,
            'positionid' : this.position.positionid,
            'levelid' : this.level.levelid,
            'educationid' : this.education.eduid,
            'joindate' : this.formatDate(this.joindate),
            'resindate' : null,
            'joincallsign' : this.formatDate(this.joindate),
            'releasecallsign' : null,
            'address' : this.address,
            'email' : this.email,
            'phonenum' : this.phonenum,
            'remark' : this.remark,
            'remark_resign' : null,
            'remark_promote' : null,
            'remark_movement' : null,
            'remark_inactive' : null,
            'statusid' : 2,
            'createdby' : localStorage.getItem('user-username'),
            'replacementemployeeid': null
          }
          this.$axios.post('/employee/createnewemployee', body, {
            headers: {
              Authorization : localStorage.getItem('user-token')
            }
          })
          .then(respone => {
            var data = respone.data
            if (data.fullname != null) {
              if(data.datecreated == null) {
                this.$q.loading.hide()
                this.$q.notify({
                  message: data.fullname,
                  color: 'red',
                  icon: 'warning'
                })
              } else {
                this.$q.notify({
                  message: 'New Employee Created',
                  color: 'secondary',
                  icon: 'check_circle'
                })
                this.onReset()
                this.$q.loading.hide()
                window.location.href = '/#/employee'
              }
            } else {
              this.$q.loading.hide()
              this.$q.notify({
                message: 'Failed to Create New Employee',
                color: 'red',
                icon: 'warning'
              })
            }
          })
          .catch(error => {
            this.$q.loading.hide()
            this.$q.notify({
              message: 'Error : ' + error,
              color: 'red',
              icon: 'warning'
            })
            console.log('message : ' + error)
            localStorage.removeItem('checknikktp')
          })
        } else {
          this.$q.loading.hide()
          this.$q.notify({
            message: 'Please Complete the Form',
            color: 'red',
            icon: 'warning'
          })
        }
      },
      getEmloyeeByCallsign(callsign) {
        this.$axios.get('/employee/getemployeebycallsign',{
          headers: {
            Authorization : localStorage.getItem('user-token')
          },
          params: {
            callsign : callsign
          }
        })
        .then(response => {
          console.log(response.data)
          if (response.data == null) {
            localStorage.setItem('checkcallsign', 'available')
            this.sleep(1000)
          } else {
            localStorage.setItem('checkcallsign', 'used')
            this.sleep(1000)
          }
        })
        .catch(error => {
          console.log('message : '+error)
          localStorage.setItem('checkcallsign', 'error')
          this.sleep(1000)
        })
      },
      getEmloyeeByNikKtp(nikktp) {
        this.$axios.get('/employee/getemployeebynikktp',{
          headers: {
            Authorization : localStorage.getItem('user-token')
          },
          params: {
            nikktp : nikktp
          }
        })
        .then(response => {
          console.log(response.data)
          if (response.data == null) {
            localStorage.setItem('checknikktp', 'available')
            this.sleep(1000)
          } else {
            localStorage.setItem('checknikktp', 'used')
            this.sleep(1000)
          }
        })
        .catch(error => {
          console.log('message : '+error)
          localStorage.setItem('checknikktp', 'eror')
          this.sleep(1000)
        })
      },
      onReset(){
          this.callsign = null
          this.nik_ktp = null
          this.nik = null
          this.fullname = null
          this.education = null
          this.address = null
          this.joindate = null
          this.phonenum = null
          this.regional = null
          this.division = null
          this.mainjob = null
          this.position = null
          this.level = null
          this.joindate = null
          this.company = null
          this.email = null
          this.remark = null
          
      },
      getDivisionList(){
        this.$axios.get('/master/getalldivision',{
          headers: {
            Authorization : localStorage.getItem("user-token")
          }
        })
        .then(response => {
          this.optionsDivision = response.data
        })
        .catch(error => {
          console.log('message : ' + error)
        })
      },
      getRegionalList(){
        this.$axios.get('/master/getallregional',{
          headers: {
            Authorization : localStorage.getItem("user-token")
          }
        })
        .then(response => {
          this.optionsRegional = response.data
        })
        .catch(error => {
          console.log('message : ' + error)
        })
      },
      getPositionList(){
        this.$axios.get('/master/getallposition',{
          headers: {
            Authorization : localStorage.getItem("user-token")
          }
        })
        .then(response => {
          this.optionsPosition = response.data
        })
        .catch(error => {
          console.log('message : ' + error)
        })
      },
      getMainjobList(){
        this.$axios.get('/master/getallmainjob',{
          headers: {
            Authorization : localStorage.getItem("user-token")
          }
        })
        .then(response => {
          this.optionsMainjob = response.data
        })
        .catch(error => {
          console.log('message : ' + error)
        })
      },
      getLevelList(){
        this.$axios.get('/master/getalllevel',{
          headers: {
            Authorization : localStorage.getItem("user-token")
          }
        })
        .then(response => {
          this.optionsLevel = response.data
        })
        .catch(error => {
          console.log('message : ' + error)
        })
      },
      getCompanyList(){
        this.$axios.get('/master/getcompany',{
          headers: {
            Authorization : localStorage.getItem("user-token")
          }
        })
        .then(response => {
          this.optionsCompany = response.data
        })
        .catch(error => {
          console.log('message : ' + error)
        })
      },
      getEducationList(){
        this.$axios.get('/master/getalledu',{
          headers: {
            Authorization : localStorage.getItem("user-token")
          }
        })
        .then(response => {
          this.optionsEducation = response.data
        })
        .catch(error => {
          console.log('message : ' + error)
        })
      },
      formatDate (date) {
        var formatDate = date.replace(/\//g, '-')
        var datearray = formatDate.split('-')
        var newdate = datearray[2] + '-' + datearray[1] + '-' + datearray[0]
        return newdate
      },
    },
    beforeMount () {
      this.getCompanyList()
      this.getDivisionList()
      this.getPositionList()
      this.getRegionalList()
      this.getMainjobList()
      this.getLevelList()
      this.getEducationList()
    }
}