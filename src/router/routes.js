
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/mainmenu/Home.vue') },
      { path: 'employee', component: () => import('pages/mainmenu/Employee.vue') },
      { path: 'report', component: () => import('pages/mainmenu/Peformance.vue') },
      { path: 'history', component: () => import('pages/mainmenu/History.vue') },
      { path: 'bin', component: () => import('src/pages/mainmenu/Bin.vue') }
    ]
  },
  {
    path: '/account',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: 'changepass', component: () => import('src/pages/security/Changepass.vue') }
    ]
  },
  {
    path: '/extended',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: 'newemployee', component: () => import('pages/extended/InsertEmp.vue') },
      { path: 'editemployee/:id/action/:action', component: () => import('pages/extended/EditEmp.vue')},
      { path: 'resignemployee/:id', component: () => import('pages/extended/ResignEmp.vue')},

    ]
  },
  {
    path: '/auth',
    component: () => import('layouts/LoginLayout.vue'),
    children: [
      {path: '', component: () => import('pages/security/Login.vue')}
    ]
  },
  {
    path: '/master',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: 'regional', component: () => import('pages/master/Regional.vue') },
      { path: 'education', component: () => import('src/pages/master/Education.vue') },
      { path: 'company', component: () => import('pages/master/Company.vue') },
      { path: 'job', component: () => import('pages/master/Job.vue') },
      { path: 'user', component: () => import('src/pages/master/User.vue') },
      { path: 'position', component: () => import('src/pages/master/Position.vue') },
      { path: 'level', component: () => import('src/pages/master/Level.vue') },
      { path: 'division', component: () => import('src/pages/master/Division.vue') },
      { path: 'bonus', component: () => import('src/pages/master/Bonus.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
