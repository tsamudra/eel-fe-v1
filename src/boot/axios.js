import Vue from 'vue'
import axios from 'axios'

Vue.prototype.$axios = axios

axios.interceptors.request.use((config) => {
  config.baseURL = 'http://localhost:9191/'
  //config.baseURL = 'https://10.123.6.172:9191/'
  const token = localStorage.getItem('user-token')

  if (token) {
    config.headers.Authorization = token
  } else {
    window.location.href = '/#/auth'
  }

  config.withCredentials = true

  return config
})

axios.interceptors.response.use(response => response, (error) => {
  let errorMessage = ''
  console.log(error)
  if (error.response !== undefined) {
    if (error.response.status === 401) {
      window.location.href = '/#/auth'
    }
  }

  if (error.message !== undefined) {
    errorMessage = error.message
  } else if (error.response !== undefined) {
    errorMessage = error.response
  } else {
    errorMessage = error
  }

  if (error.response !== undefined) {
    if (error.response.data !== undefined) {
      if (error.response.data.message !== undefined) {
        errorMessage = error.response.data.message
      }
    }
  }

  return Promise.reject(errorMessage)
})
